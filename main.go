package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
)

func checkP(e error) {
	if e != nil {
		panic(e)
	}
}

func check(e error) {
	if e != nil {
		fmt.Print(e)
	}
}

func getToken() string {
	t, err := ioutil.ReadFile("tokenfile")
	checkP(err)
	token := string(t)
	l := len(token)
	if token[l-1] == '\n' {
		return token[0 : l-1]
	}
	return token
}

func main() {

	discord, err := discordgo.New("Bot " + getToken())
	checkP(err)

	discord.AddHandler(memberJoin)
	discord.AddHandler(messageCreate)

	err = discord.Open()
	checkP(err)
	fmt.Println("Ready!")

	_, err = discord.ChannelMessageSend("the channel ID to which the message should be sent", "work!!!!")
	checkP(err)

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	err = discord.Close()
	checkP(err)
}

func memberJoin(s *discordgo.Session, e *discordgo.GuildMemberAdd) {

	err := s.GuildMemberRoleAdd(e.GuildID, e.User.ID, "rank given at the entrance")
	check(err)

}

func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	if m.Author.ID == s.State.User.ID {
		return
	}
	if m.Author.Bot {
		return
	}

	if m.Content == "!"+"site" {
		s.ChannelMessageSend(m.ChannelID, "**Website** - https://google.com")
	}
	if m.Content == "<@id bot>" {
		s.ChannelMessageSend(m.ChannelID, "Cze")
	}
}
