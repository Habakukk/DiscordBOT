# WojtekMBOT
Discord bot in GOLang

The bot was written by [Bopke](https://git.mkubik.ovh/MKubik/RHbot) and modified by [WojtekM](https://wojtekm.com)

### התקנה
* התקן בהתאם ``go get github.com/bwmarrin/discordgo`` 
* לקמפל
* הדבק את האסימון שלך לקובץ חדש ``tokenfile``
* הפעל את הבוט ``./WojtekMBOT``
